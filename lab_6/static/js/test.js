$( document ).ready(function() {
    var button_6 = $('button:contains("6")');
    var button_3 = $('button:contains("3")');

    var button_clear = $('button:contains("AC")');
    var button_res = $('button:contains("=")');

    var button_add = $('button:contains("+")');
    var button_sub = $('button:contains("-")');
    var button_mul = $('button:contains("*")');
    var button_div = $('button:contains("/")');

    var button_log = $('button:contains("log")');
    var button_sin = $('button:contains("sin")');
    var button_tan = $('button:contains("tan")');

    QUnit.test( "Addition Test", function( assert ) {
      button_6.click();
      button_add.click();
      button_3.click();
      button_res.click();
      assert.equal( $('#print').val(), 9, "6 + 3 = 9" );
      button_clear.click();
    });

    QUnit.test( "Substraction Test", function( assert ) {
        button_6.click();
        button_sub.click();
        button_3.click();
        button_res.click();
        assert.equal( $('#print').val(), 3, "6 - 3 = 3" );
        button_clear.click();
      });

    QUnit.test( "Multiply Test", function( assert ) {
        button_6.click();
        button_mul.click();
        button_3.click();
        button_res.click();
        assert.equal( $('#print').val(), 18, "6 * 3 = 18" );
        button_clear.click();
    });

    QUnit.test( "Division Test", function( assert ) {
        button_6.click();
        button_div.click();
        button_3.click();
        button_res.click();
        assert.equal( $('#print').val(), 2, "6 / 3 = 2" );
        button_clear.click();
    });

    QUnit.test( "Logartihmic Test", function( assert ) {
        button_6.click();
        button_log.click();
        assert.equal( $('#print').val(), 0.9030899869919435 , "6 log10 = 0.778151250383644" );
        button_clear.click();
    });

    QUnit.test( "Sinus Test", function( assert ) {
        button_6.click();
        button_sin.click();
        assert.equal( $('#print').val(), 0.9893582466233818 , "sin 6 = -0.27941549819892586" );
        button_clear.click();
    });

    QUnit.test( "Tangen Test", function( assert ) {
        button_6.click();
        button_tan.click();
        assert.equal( $('#print').val(), -6.799711455220379 , "tan 6 = -0.29100619138474915" );
        button_clear.click();
    });

});
