from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, bio_dict, mhs_name
from django.http import HttpRequest
from unittest import skip

class Lab2AddonUnitTest(TestCase):

    def test_lab_2_addon_url_is_exist(self):
        response = Client().get('/lab-2-addon/')
        self.assertEqual(response.status_code, 200)

    def test_root_url_now_is_using_index_page_from_lab_2(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
       

   